#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

const args = process.argv.slice(2);

if ( args.length == 0 ) {
    // the paramter will be index number of the Fibonacci sequence
    console.log("Usage: rpc_client.js num");
    process.exit(1);
}

amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
        ch.assertQueue('', {exclusive: true}, function(err, q) {
            const uuid = generateUuid();
            // the paramter will be index number of the Fibonacci sequence
            const num = parseInt(args[0]);

            console.log(' [x] Requesting fib(%d)', num);

            // listen for the response from the RPC server
            // the RPC server will write the response to the queue specified in replyTo
            ch.consume(q.queue, function(msg) {
                // check if it'sth correct request based on the correlationId
                if ( msg.properties.correlationId == uuid ) {
                    console.log(' [.] Got %s', msg.content.toString());
                    setTimeout(function() {
                        conn.close();
                        process.exit(0);
                    }, 500);
                }
            }, {noAck: true});

            // send the Fibonacci index number to be processed by the RPC server
            // need to provide the queue name for the RPC server to reply back to
            // also needs the correlationId so it will know which request to respond to
            ch.sendToQueue('rpc_queue', 
                new Buffer(num.toString()), 
                { correlationId: uuid, replyTo: q.queue }
            );
        });
    });
});


/**
 * Generate the unique user id
 */
function generateUuid() {
    let uuid = '';

    for ( let i = 0; i < 3; ++i ) {
        uuid += Math.random().toString();
    } //end loop

    return uuid;
}