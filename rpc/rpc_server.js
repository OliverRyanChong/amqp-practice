#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel( function (err, ch) {
        const q = 'rpc_queue';
        ch.assertQueue(q, {durable: false});
        ch.prefetch(1);

        console.log(' [x] Awaiting RPC requests');

        ch.consume(q, function reply(msg) {
            const num = parseInt(msg.content.toString());

            console.log(" [.] fib(%d)", num);

            // calculate the Fibonacci value
            const fibVal = fib(num);

            // send the processed result back to the client
            // the client queue is based on replyTo
            // the request is identified by the correlationId
            ch.sendToQueue(msg.properties.replyTo, 
                new Buffer(fibVal.toString()),
                {correlationId: msg.properties.correlationId}
            );

            ch.ack(msg);
        });
    });
});


const cache = {};
/**
 * Calculate the Fibonacci sequence
 * @param {int} n 
 */
const fib = n => {
    if ( n == 0 || n == 1 ) {
        return n;
    } else {
        // memoize (check if in cache)
        if ( n in cache ) {
            return cache[n];
        } else {
            // recursive call
            let val = fib(n-2) + fib(n-1);
            // memoize (by storing in cache)
            cache[n] = val;
            return val;
        }
    }
};