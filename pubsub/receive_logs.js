#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel( function(err, ch) {
        const ex = 'logs';

        ch.assertExchange(ex, 'fanout', {durable: false});

        ch.assertQueue('', {exclusive: true}, function(err, q) {
            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
            ch.bindQueue(q.queue, ex, '');
            ch.consume(q.queue, function(msg) {
                console.log(' [x] Consume msg(%s) from pubsub exchange(%s) and queue(%s)', msg.content.toString(), ex, q.queue);
            }, {noAck: true});
        });
    });
});